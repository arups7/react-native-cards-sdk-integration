import PropTypes from 'prop-types';
import React from 'react';
import { requireNativeComponent } from 'react-native';



var CardUI = requireNativeComponent('SuperCard');

export default class CardView extends React.Component {
    render() {
      return <CardUI {...this.props} />;
    }
 }