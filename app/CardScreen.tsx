import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Button,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import CardView from './components/CardComponent';

import { NativeModules } from 'react-native';

const SuperCard = NativeModules.SuperCard;



const constants = {
  CARD_ID: "6d46b117-2b03-47d6-9235-f1f7288a0492"
};



declare const global: {HermesInternal: null | {}};

export default class CardScreen extends Component  {
  state = {isCardNotHidden: false, cardPan: "", isStoreSetUp: false};

  callSecureStoreSetUp = () => {
    SuperCard.setUpSecuredStore();
  };

  getCardPan = async () => {
    try {
      var cardPan = await SuperCard.getCardPanForId(constants.CARD_ID)

      console.log(this.state);
      console.log(cardPan);
      this.setState({cardPan: cardPan});
    } catch(e) {
      console.error(e);
    }
  };

  showCard = async () => {
    try {
      await SuperCard.showCard(constants.CARD_ID);
      this.setState({isCardNotHidden: true});
    } catch(e) {
      console.error(e);
    }
  };

  render () {
    return (
      <View
        style={this.styles.scrollView}>
        {global.HermesInternal == null ? null : (
          <View style={this.styles.engine}>
            <Text style={this.styles.footer}>Engine: Hermes</Text>
          </View>
        )}
        <View style={this.styles.body}>
          <View style={this.styles.sectionContainer}>
            <Text style={this.styles.sectionTitle}>Step One</Text>
            <Button onPress={()=>{ SuperCard.setUp()}} title="Set Up"/>
          </View>
          <View style={this.styles.sectionContainer}>
            <Text style={this.styles.sectionTitle}>Step Two</Text>
            <Button onPress={this.callSecureStoreSetUp} title="Set Up Secured Store"/>
          </View>
          <View style={this.styles.sectionContainer}>
            <Text style={this.styles.sectionTitle}>TRY</Text>
            <Button onPress={this.getCardPan} title="Get Card Pan"/>
            <Text style={this.styles.sectionDescription}>{this.state.cardPan}</Text>
          </View>
          <View style={this.styles.sectionContainer}>
            <Text style={this.styles.sectionTitle}>TRY</Text>
            {this.state.isCardNotHidden ? <CardView/> : <Button onPress={this.showCard} title="Show Card"/>}
          </View>
        </View>
      </View>
    );
  }

  styles = StyleSheet.create({
    scrollView: {
      backgroundColor: Colors.lighter,
    },
    engine: {
      position: 'absolute',
      right: 0,
    },
    placeholder: {
      position: 'absolute',
      right: 0,
    },
    body: {
      backgroundColor: Colors.white,
    },
    sectionContainer: {
      marginTop: 32,
      paddingHorizontal: 24,
    },
    sectionTitle: {
      fontSize: 24,
      fontWeight: '600',
      color: Colors.black,
    },
    sectionDescription: {
      marginTop: 8,
      fontSize: 18,
      fontWeight: '400',
      color: Colors.dark,
    },
    highlight: {
      fontWeight: '700',
    },
    footer: {
      color: Colors.dark,
      fontSize: 12,
      fontWeight: '600',
      padding: 4,
      paddingRight: 12,
      textAlign: 'right',
    },
  })
};