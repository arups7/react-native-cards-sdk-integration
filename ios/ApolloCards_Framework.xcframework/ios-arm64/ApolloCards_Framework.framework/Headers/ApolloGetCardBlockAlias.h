#import <Foundation/Foundation.h>
#import "ApolloCard.h"
#import "ApolloCardErrorInfo.h"

typedef void (^ApolloGetCardSuccessBlock)(ApolloCard *card);
typedef void (^ApolloGetCardPanSuccessBlock)(NSString *cardPan);

typedef void (^ApolloCardFailureBlock)(ApolloCardErrorInfo *userInfo);

typedef void (^ApolloSetPinSuccessBlock)(void);
