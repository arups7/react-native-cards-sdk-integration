#import <Foundation/Foundation.h>

@interface ApolloBinRange : NSObject<NSCoding>

@property (nonatomic, readonly) NSString *bin;
@property (nonatomic, readonly) NSString *range;

- (instancetype)initWithBin:(NSString *)bin
                   andRange:(NSString *)range;

@end
