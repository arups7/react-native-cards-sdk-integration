#ifndef SecureStore_h
#define SecureStore_h

typedef void (^ApolloCardEmptyBlockType)(void);
typedef void (^ZETAErrorBlockType)(NSError *error);
@protocol ApolloCardSecureStore <NSObject>

- (BOOL)isStoreSetUpSuccessfully;
- (void)setData:(NSData *)data forKey:(NSString *)key completion:(ZETAErrorBlockType)completion;
  
- (void)dataForKey:(NSString *)key
        userPrompt:(BOOL)promptUser
        completion:(void (^)(NSError *error, NSData *data))completion;
  
- (void)resetAllData;

@end

#endif /* SecureStore_h */
