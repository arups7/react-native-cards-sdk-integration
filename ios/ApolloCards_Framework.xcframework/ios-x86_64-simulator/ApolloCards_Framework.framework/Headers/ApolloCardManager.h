#import <Foundation/Foundation.h>
#import "ApolloCard.h"
#import "ApolloGetCardBlockAlias.h"
#import "ApolloCardSecureStore.h"
#import <UIKit/UIKit.h>

typedef void (^SuperPinFetchBlock)(NSString * _Nullable superPin, NSTimeInterval validityInSeconds, ApolloCardErrorInfo * _Nullable error);

@interface ApolloCardManager: NSObject

- (nonnull instancetype)init;

- (void) registerSDKWithAuthToken:(nonnull NSString *) authToken NS_SWIFT_NAME(registerSDK(authToken:));

-(void)setUpSDKWithAccountHolderID:(nonnull NSString *)accountHolderID;

- (void)getCardWithCardID:(nonnull NSString *)cardID
                  success:(nullable ApolloGetCardSuccessBlock)success
                  failure:(nullable ApolloCardFailureBlock)failure;

-(void) setCardPinForCardId:(nonnull NSString *)cardID
                    success:(nullable ApolloSetPinSuccessBlock)success
                    failure:(nullable ApolloCardFailureBlock)failure;

- (void)getCardWithCardID:(nonnull NSString *)cardID
                authToken:(nonnull NSString *)authToken
                  success:(nullable ApolloGetCardSuccessBlock)success
                  failure:(nullable ApolloCardFailureBlock)failure __attribute__((deprecated("use getCardWithCardID:success:failure: instead")));


-(void)setUpSecureStoreWithSuccess:
(nullable ApolloCardEmptyBlockType)successBlock
                                     failureBlock:(nullable ApolloCardEmptyBlockType)failureBlock;
-(void) setUpSecureStore:(nonnull id<ApolloCardSecureStore>) secureStore;
-(void)syncCardDetails:(nonnull NSString *)cardId
               success:(nullable ApolloCardEmptyBlockType)success
               failure:(nullable ApolloCardFailureBlock)failure;

- (void)logout;

-(BOOL)isSecureStoreSetupDone;
-(void)getCardPanFromCardID:(nonnull NSString *)cardID
                           success:(nullable ApolloGetCardPanSuccessBlock)successBlock
                           failure:(nullable ApolloCardFailureBlock)failure;
- (void)generateTOTPWithCompletion:(nullable SuperPinFetchBlock) completion;

-(void)addCardViewOnContainer:(nonnull UIView *)view
                       cardID:(nonnull NSString *)cardID;

-(void)getCardUIWithCardID:(nonnull UIView *)view
                    cardID:(nonnull NSString *)cardID;
@end
