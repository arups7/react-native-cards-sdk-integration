#import <Foundation/Foundation.h>
#import "ApolloBinRange.h"
#import "ApolloSensitiveView.h"
#import "ApolloDecryptedCardDetails.h"

@interface ApolloCard : NSObject<NSCoding>

@property (nonatomic, readonly) NSString *cguid;
@property (nonatomic, readonly) NSString *crn;
@property (nonatomic, readonly) NSString *cardType;
@property (nonatomic, readonly) NSString *maskedPan;
@property (nonatomic, readonly) NSString *cardStatus;
@property (nonatomic, readonly) ApolloBinRange *binRange;
@property (nonatomic, readonly) NSArray<NSString *> *vectors;
@property (nonatomic, readonly) ApolloSensitiveView *sensitiveView;
@property (nonatomic, readonly) ApolloDecryptedCardDetails *cardDetails __deprecated_msg("use rawCardDetails instead");
@property (nonatomic, readonly) NSDictionary *rawCardDetails;

- (instancetype)initWithCguid:(NSString *)cguid
                          crn:(NSString *)crn
                     cardType:(NSString *)cardType
                    maskedPan:(NSString *)maskedPan
                   cardStatus:(NSString *)cardStatus
                     binRange:(ApolloBinRange *)binRange
                      vectors:(NSArray<NSString *> *)vectors
                sensitiveView:(ApolloSensitiveView *)sensitiveView
                  cardDetails:(ApolloDecryptedCardDetails *)cardDetails;

- (instancetype)initWithCguid:(NSString *)cguid
                          crn:(NSString *)crn
                     cardType:(NSString *)cardType
                    maskedPan:(NSString *)maskedPan
                   cardStatus:(NSString *)cardStatus
                     binRange:(ApolloBinRange *)binRange
                      vectors:(NSArray<NSString *> *)vectors
                sensitiveView:(ApolloSensitiveView *)sensitiveView
                  cardDetails:(ApolloDecryptedCardDetails *)cardDetails
               rawCardDetails:(NSDictionary *) rawCardDetails;

-(nullable NSData *)data;
+(nullable instancetype)getObjectFromData:(nullable NSData *)data;
@end

