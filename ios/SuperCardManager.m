//
//  SuperCards.m
//  AwesomeTSProject
//
//  Created by arup.s on 06/09/20.
//

#import "SuperCardManager.h"
#import "AppDelegate.h"


@interface SuperCardManager()<ApolloCardSecureStore>
@property (nonatomic, weak) ApolloCardManager *cardManager;
@property (nonatomic, strong) UIView *containerView;
@end


@implementation SuperCardManager
RCT_EXPORT_MODULE(SuperCard);

RCT_EXPORT_METHOD(setUp) {
  self.cardManager = [(AppDelegate *)[UIApplication sharedApplication].delegate cardManager];
  dispatch_async(dispatch_get_main_queue(), ^{
    self.containerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width * 0.75, [UIScreen mainScreen].bounds.size.width * 0.47)];
  });
  
}

RCT_EXPORT_METHOD(setUpSecuredStore) {
  [self.cardManager setUpSecureStore:self];
}

RCT_EXPORT_METHOD(getCardPanForId:(NSString* ) cardId withReolver:(RCTPromiseResolveBlock) resolve rejecter:(RCTPromiseRejectBlock) reject) {
  [self.cardManager getCardPanFromCardID:cardId success:^(NSString *cardPan) {
    resolve(cardPan);
  } failure:^(ApolloCardErrorInfo *userInfo) {
    reject(userInfo.code, userInfo.message, nil);
  }];
}

RCT_EXPORT_METHOD(showCard:(NSString *) cardId withReolver:(RCTPromiseResolveBlock) resolve rejecter:(RCTPromiseRejectBlock) reject ) {
  dispatch_async(dispatch_get_main_queue(), ^{
    [self.cardManager addCardViewOnContainer:self.containerView cardID:cardId];
    resolve(@(YES));
  });
}

- (UIView *)view {
  return _containerView;
}


- (void)dataForKey:(NSString *)key userPrompt:(BOOL)promptUser completion:(void (^)(NSError *, NSData *))completion {
  completion(nil, [[NSUserDefaults standardUserDefaults] valueForKey:key]);
}

- (BOOL)isStoreSetUpSuccessfully {
  return YES;
}

- (void)resetAllData {
  
}

- (void)setData:(NSData *)data forKey:(NSString *)key completion:(ZETAErrorBlockType)completion {
  [[NSUserDefaults standardUserDefaults] setValue:data forKey:key];
  completion(nil);
}

@end
