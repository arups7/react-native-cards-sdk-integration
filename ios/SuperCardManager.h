//
//  SuperCards.h
//  AwesomeTSProject
//
//  Created by arup.s on 06/09/20.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>
#import <React/RCTViewManager.h>

NS_ASSUME_NONNULL_BEGIN

@interface SuperCardManager : RCTViewManager

@end

NS_ASSUME_NONNULL_END
