#import <React/RCTBridgeDelegate.h>
#import <UIKit/UIKit.h>
#import <ApolloCards_Framework/ApolloCards_Framework.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, RCTBridgeDelegate>

@property (nonatomic, strong) UIWindow *window;
@property (nonatomic) ApolloCardManager *cardManager;
@end
